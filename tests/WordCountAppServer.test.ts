// Load from package
import supertest from 'supertest';
import mongoose from 'mongoose';
import { response } from 'express';

// Load from source code
import WordCountApp from '../src/apis/WordCountApp';
import WordCountModel from '../src/apis/models/WordCountModel';
import Const from '../src/utils/Const';



describe('Test for API endpoint on WordCountServer', () => {

  // Declare variables to use for test
  let wordCountApp;
  let request: supertest.SuperTest<supertest.Test>;

  // Define constants for test
  const URL = '/wordcount';

  beforeAll(async () => {

    wordCountApp = new WordCountApp();
    wordCountApp.bootstrap();
    request = supertest(wordCountApp.app);

  })

  it('will request POST method and success', (done)  => {
    request
      .post(`${URL}`)
      .set('Content-Type', 'application/json')
      .send({
        'word': 'fit',
        'url': 'https://www.virtusize.jp/'
      })
      .then((response) => {

        const resBody = response.body;
        
        expect(resBody).toHaveProperty("status");
        expect(resBody.status === 200 || resBody.status === 201).toBeTruthy();
        expect(resBody).toHaveProperty("message");
        expect(resBody.message).toHaveProperty("count");
        expect(typeof resBody.message.count).toBe('number');

        done();
      })
  });

  it('will request POST method and fail', (done)  => {
    request
      .post(`${URL}`)
      .set('Content-Type', 'application/json')
      .send({})
      .then((response) => {

        const resBody = response.body;
        
        console.log(resBody.message);
        expect(resBody).toHaveProperty("status");
        expect(resBody.status).toBe(400);
        expect(resBody).toHaveProperty("message");
        expect(resBody.message.error).toBe(Const.POST_NO_FIELD_ERROR);
        expect(resBody.message.request).toMatchObject(Const.POST_REQUEST_FORMAT);

        done();
      })
  });

  afterAll( async () =>{
    await mongoose.connection.close();
  })

})
