# API Enpoints
This file explains API endpoint usage.
<br/>
<br/>

## ***HTTP POST Request***

### [PATH]
&ensp;&ensp;`POST /wordcount`
    

### [Request Format]
&ensp;&ensp; - Content-Type :
application/json

&ensp;&ensp; - Body :

Name          | Type           | Description                    
------------- | -------------- |------------------------------ 
`word`        | string         | **Required** Target word to be counted on URL
`url`        | string         | **Required** Target URL to be parsed and counted with word

&ensp;&ensp;(Example)

    {
      "word": "fit",
      "url": "https://www.virtusize.jp/"
    }


### [Response Format]
   
&ensp;&ensp; - Success Content :


Name                | Type           | Description                    
------------------- | -------------- |------------------------------ 
`status`       | integer        | HTTP status code
`message`    | string         | Message including the result of word counting


&ensp;&ensp;(Example)

    {
      "status": 200,
      "message": : {
        "count": 2
      }
    }
  
&ensp;&ensp; - Error Content : 

Name                | Type           | Description                    
------------------- | -------------- |------------------------------ 
`status`       | integer        | HTTP status code
`message`    | string         | Message including error information

&ensp;&ensp;(Example)

    {
      "status": 400,
      "message": {
        "error": "Field content on body does not exist",
        "request": {
          "method": "POST",
          "url": "/wordcount",
          "contentType": "application/json",
          "body": {
            "word": "fit",
            "url": "https://www.virtusize.jp/"
          }
        }
      }
    }

<br/>
<br/>

