# virtusize-asmt-backend

This is a frontend UI to be implemented based on the assignment.

<br/>

## ***Requirement***
`yarn` should be installed. Please check out set up guide below.

&ensp;&ensp; - Link for set up guide :
[yarn](https://yarnpkg.com/lang/en/docs/install)
<br/>
<br/>

## ***Set up for dependency***
To properly runnning, please run a command below.

### `yarn install`
<br/>
<br/>

## ***Run for development***
Runs the app in the development mode.

### `yarn start`

It will run on [http://localhost:8888](http://localhost:8888).
<br/>
<br/>

## ***Run for test***
Runs unit test.

### `yarn test`
<br/>
<br/>
