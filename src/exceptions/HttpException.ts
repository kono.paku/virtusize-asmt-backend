/** Class representing HttpException **/
class HttpException extends Error {

  // Declare variables set by constructor
  public status: number;
  public message: string;

  /**
   * Create HttpException including status code and message on error
   * 
   * @constructor
   * @param {number} status - status code on error
   * @param {string} message - message on error
   */
  constructor(status: number, message: string) {

    super(message);
    this.status = status;
    this.message = message;

  }
}
 
export default HttpException;
