// Load from source code
import WordCountApp from "./apis/WordCountApp";

let wordCountApp;

const { PORT = 8888 } = process.env;

try {

  wordCountApp = new WordCountApp();
  wordCountApp.bootstrap();
  wordCountApp.app.listen(PORT, () => {
    console.log('Server started at http://localhost:' + PORT);
  });

} catch (err) {

  console.log("[CRITICAL] At creating and bootstraping on WordCountApp! Error stack is >> ");
  console.log(err);
  process.exit(-1);

}
