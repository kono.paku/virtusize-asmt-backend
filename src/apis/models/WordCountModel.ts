// Load from package
import { Document, Model, model, Schema } from "mongoose";

/** 
 * Interface representing WordCount
 * 
 * @extends Document 
 */
export interface WordCount extends Document {
  id: Schema.Types.ObjectId;
  word: string;
  url: string;
  number: number;
}

// Define a WordCountSchema for mongoDB
const wordCountSchema: Schema = new Schema(
  {
    word: {
      type: String,
      required: true
    },
    url: {
      type: String,
      required: true
    },
    count: {
      type: Number,
      required: true
    },
  }
);

// Create WordCountModel instance depending on WordCountSchema
const WordCountModel: Model<Document<WordCount>> = model("WordCount", wordCountSchema);

export default WordCountModel;
