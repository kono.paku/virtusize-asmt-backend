// Load from package
import express from 'express';
import { Request, Response, NextFunction } from 'express';
import bodyParser from 'body-parser';
import mongoose from 'mongoose';
import timeout from 'connect-timeout';
import morgan from 'morgan';
import config from 'config';

// Load from source code
import WordCountController from './controllers/WordCountController';
import HttpException from '../exceptions/HttpException';
import Const from '../utils/Const';

/** Class representing WordCountApp **/
export default class WordCountApp {

  // Initialize variables set by constructor
  public app: express.Application;
  private router: express.Router;
  
/**
 * Create WordCountApp and set Express app and router
 * 
 * @constructor
 */
constructor() {

    this.app = express();
    this.router = express.Router();
  }

  /**
   * Bootstrap for resolving dependency and config for WordCountApp
   */
  public bootstrap () {

    // Connect MongoDB
    this.connectMongoDB();
    
    // Set configuration for Express app
    this.setConfig();
    
    // Add routing logic
    this.addRouting();

    // Add error handling logic
    this.addErrorHandling();

  }

  /**
   * Connect to MongoDB Altas with options
   */
  private connectMongoDB () {
    const connectFullPath = `mongodb+srv://${config.get('MONGO_ATLAS.ID')}:${config.get('MONGO_ATLAS.PW')}@${config.get('MONGO_ATLAS.URL')}`;
    
    mongoose.connect(
      connectFullPath,
      {
        useNewUrlParser: true,
        useUnifiedTopology: true
      },
    );
  }

  /**
   * Set configuration morgan, body-parser
   */
  private setConfig() {
    // Set timeout value as 10 seconds
    this.app.use(timeout('10s'));

    // Show time-elapse for api call in dev enviroment
    this.app.use(morgan('dev'));

    // Set body-parser encoding options as querystring
    //
    // <Reference> https://stackoverflow.com/questions/29960764/what-does-extended-mean-in-express-4-0/45690436#45690436
    this.app.use(bodyParser.urlencoded({extended: false}));
    this.app.use(bodyParser.json());
  }

  /**
   * Add routing logic
   */
  private addRouting() {
    // Create fileContoller to route
    const wordCountController = new WordCountController(this.router);

    // Routes to file path router
    this.app.use('/', wordCountController.routes());
  }

  /**
   * Add error handling logic for HTTP exceptions
   */
  private addErrorHandling() {
    // Add a exception handler for an unexpected URL path
    this.app.use((req: Request, res: Response, next: NextFunction) => {
      let error;
      if (!Const.HTTP_ALLOWED_METHOD.includes(req.method)) {
        error = new HttpException(
          Const.HTTP_NOT_ALLOWED_METHOD_STATUS,
          Const.NOT_ALLOWED_METHOD_ERROR + Const.HTTP_ALLOWED_METHOD
        );
      } else {
        error = new HttpException(
          Const.HTTP_BAD_REQUEST_STATUS,
          Const.NOT_VALID_PATH_ERROR
        );
      }
      next(error);
    });
    
    // Add a exception handler for errors including internal sever
    this.app.use((error: HttpException, req:Request, res: Response, next: NextFunction) => {
      const statusCode = error['status'] || 500;
      res.status(statusCode).json({
        status: statusCode,
        message: {
          error: error.message
        }
      });
    });
  }
}
