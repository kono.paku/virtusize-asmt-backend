// Load from package
import { Request, Response, NextFunction } from 'express';
import got, { Response as GotResponse } from 'got';
import { Node, NodeType, parse } from 'node-html-parser';
import mongoose, { Document } from 'mongoose';

// Load source code
import WordCountModel, { WordCount } from '../models/WordCountModel';
import Const from '../../utils/Const';

/** Class representing WordCountService **/
export default class WordCountService {
  
  /**
   * Route to the WordCountService instance depending on HTTP POST method request 
   * 
   * @param {Request} request -  HTTP POST method request
   * @param {Response} response - HTTP POST method response
   */
  public postWordCounts = (async (request: Request, response: Response, nextFunction: NextFunction) =>  {
    
    // Validate whether headers and content-type exists
    if (!request.headers
      || !request.headers['content-type']) {

      return response.status(Const.HTTP_BAD_REQUEST_STATUS).json({ 
        status: Const.HTTP_BAD_REQUEST_STATUS,
        message: {
          error: Const.POST_NO_HEADER_ERROR,
          request: Const.POST_REQUEST_FORMAT
        },
      });
    }
    
    // Validate whether content-type is application/json
    if (request.headers['content-type']
      && !request.headers['content-type'].startsWith('application/json')) {

      return response.status(Const.HTTP_BAD_REQUEST_STATUS).json({ 
        status: Const.HTTP_BAD_REQUEST_STATUS,
        message: {
          error: Const.POST_HEADER_CONTENT_TYPE_ERROR,
          request: Const.POST_REQUEST_FORMAT
        }
      });

    }

    // Validate whether content-type is application/json
    if (!('word' in request.body)
      || !('url' in request.body)
      || !(request.body['word']) 
      || !(request.body['url'])) {

      return response.status(Const.HTTP_BAD_REQUEST_STATUS).json({ 
        status: Const.HTTP_BAD_REQUEST_STATUS,
        message: {
          error: Const.POST_NO_FIELD_ERROR,
          request: Const.POST_REQUEST_FORMAT
        }
      });

    }

    let wordCount;
    const url = request.body['url'];
    const word = request.body['word'];

    // Search for cached data on DB
    let doc = await WordCountModel
      .findOne(
        {
          'word': word,
          'url': url
        }
      )
      .exec()
      .catch((error) => {
      return response.status(Const.HTTP_INTERNAL_SERVER_STATUS).json({ 
          status: Const.HTTP_INTERNAL_SERVER_STATUS,
          message: {
            error: Const.QUERYING_ON_DB_ERROR,
            detail: error.message
          }
        });  
      })

    // Return result if it exists
    if(doc) {
    return response.status(Const.HTTP_OK_HTTP_STATUS).json({
        status: Const.HTTP_OK_HTTP_STATUS,
        message: {
          count: doc.get('count'),
        },
      });
    }

    // Procees get HTML document from url unless no cached data exists
    await got(url).then(res => {
      const htmlElement = this.getHtmlElement(res);
      const parsedTextList = this.parseHtmlElement(htmlElement);
      wordCount = this.getWordCount(word, parsedTextList);
    }).catch((error) => {
      return response.status(Const.HTTP_INTERNAL_SERVER_STATUS).json({ 
        status: Const.HTTP_INTERNAL_SERVER_STATUS,
        message: {
          error: Const.ON_RETRIEVING_ERROR,
          detail: error.message
        }
      });  
    }
    );

    const wordCountModel = new WordCountModel({
        _id: new mongoose.Types.ObjectId(),
        word: word,
        url: url,
        count: wordCount,
    });

    wordCountModel
      .save()
      .then(
        (result: Document<WordCount>) => {
          return response.status(Const.HTTP_CREATED_HTTP_STATUS).json({
            status: Const.HTTP_CREATED_HTTP_STATUS,
            message: {
              count: result.get('count'),
            },
          });
        }
      ).catch(
        (error: Error) => {
          return response.status(Const.HTTP_INTERNAL_SERVER_STATUS).json({
            status: Const.HTTP_INTERNAL_SERVER_STATUS,
            message: {
              error: Const.FAILED_TO_SAVE_ON_DB_ERROR,
              detail: error.message
            }
          });
        }
      )
  
  });

  /**
   * Create HTML Node for being parsed
   * 
   * @param {GotResponse} gotResponse - HTML Response from URL by using got
   * @return {Node} - HTML Node to be pared
   */
  private getHtmlElement = (gotResponse: GotResponse<string>): Node => {
    return parse(gotResponse.body, {
      lowerCaseTagName: false,
      comment: false,
      blockTextElements: {
        script: false,
        noscript: false,
        style: false,
        pre: false
      }
    });
  }

  /**
   * Parse HTML Node
   * 
   * @param {Node} htmlElement - HTML Node to be pared
   * @return {string[]} - List to include parsed words
   */
  private parseHtmlElement = (htmlElement: Node): string[] => {
    let resultList: string[] = [];
    htmlElement.childNodes.forEach((targetNode: Node) => {
      
      // Loop again, if there are any children
      if(targetNode.childNodes.length !== 0) {
        const subResultList = this.parseHtmlElement(targetNode);
        resultList = [...resultList, ...subResultList];
      }

      // Added a parsed text into array
      if(targetNode.nodeType === NodeType.TEXT_NODE && targetNode.textContent) {
        resultList.push(targetNode.textContent);
      }

    })

    return resultList;
  }

  /**
   * Get a word count from List
   * 
   * @param {string} word - Target word to  be checked
   * @param {string[]} list - List to include parsed words
   * @return {number} - Word count number as result
   */
  private getWordCount = (word: string, list : string[]): number => {
    let wordCount = 0;
    list.join(' ').split(/[^\w\d]+/).forEach(listElement => {
      if(listElement.toLocaleLowerCase() === word) {
        ++wordCount;
      }
    })
    return wordCount;
  }

}
