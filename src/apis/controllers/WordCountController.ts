// Load from package.
import { Router } from 'express';

// Load from source code.
import WordCountService from '../services/wordCountService';

/** Class representing WordCountController **/
export default class WordCountContoller {

  // Declare variables set by constructor
  private wordCountService: WordCountService;

  /**
   * Set Router instance and create WordCountService instance
   * 
   * @constructor
   * @param {Router} router - Router instance from WordCountService
   */
  constructor(private router: Router) {
    // Create WordCountService instance
    this.wordCountService = new WordCountService();
  }

  /**
   * Route to the WordCountService instance depending on HTTP request method
   * 
   * @return {Router} Router instance to WordCountService after inserting the routing logic
   */
  public routes() {

    this.router.post('/wordcount', this.wordCountService.postWordCounts);
    
    return this.router;

  }

}

