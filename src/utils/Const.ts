/** Class representing Const **/
class Const {
  public static readonly HTTP_ALLOWED_METHOD = ['POST'];
  public static readonly HTTP_OK_HTTP_STATUS = 200;
  public static readonly HTTP_CREATED_HTTP_STATUS = 201;
  public static readonly HTTP_BAD_REQUEST_STATUS = 400;
  public static readonly HTTP_NOT_FOUND_STATUS = 404;
  public static readonly HTTP_NOT_ALLOWED_METHOD_STATUS = 405;
  public static readonly HTTP_INTERNAL_SERVER_STATUS = 500;
 
  public static readonly NOT_ALLOWED_METHOD_ERROR = 'Not allowed method! Allowed method : ';
  public static readonly NOT_VALID_PATH_ERROR = 'Not valid Path! Please try on /wordcount';
  public static readonly POST_NO_HEADER_ERROR = 'Header or Content-type does not exist!';
  public static readonly POST_HEADER_CONTENT_TYPE_ERROR = 'Content-type is not application/json!';
  public static readonly POST_NO_FIELD_ERROR = 'Field content on body does not exist';
  public static readonly QUERYING_ON_DB_ERROR = 'Error occurs on querying on DB';
  public static readonly ON_RETRIEVING_ERROR = 'Error occurs on retreiving HTML document of URL';
  public static readonly FAILED_TO_SAVE_ON_DB_ERROR = 'Failed to save result on DB';

  public static readonly POST_REQUEST_FORMAT = {method: 'POST',url: '/wordcount',contentType: 'application/json',body: {word: 'fit',url: 'https://www.virtusize.jp/'}};  
}

export default Const;